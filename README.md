# BreadboadRecorder

This project is intended to collect audio samples from an Adafruit MAX4466 breakout board and store them to an SD card.
The project will probably be written as an mbed-cli program and will use the following hardware:

* Adafruit MAX4466 breakout board
* Some SD card breakout board (details when I find it)
* One of my STM32 breakout boards
